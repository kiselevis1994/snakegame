// Fill out your copyright notice in the Description page of Project Settings.


#include "DoubleFood.h"
#include "Snake.h"

// Sets default values
ADoubleFood::ADoubleFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADoubleFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoubleFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADoubleFood::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(interactor);

		if (IsValid(Snake))
		{
			Snake->AddSnakeElementsInTheEnd(2); 
			this->Destroy();

		}
	}
}


