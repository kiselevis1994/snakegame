// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElementBase;
class AFood;
class ADoubleFood;
class ASpeedUpFood;
class ASlowDown;
class AMine;

UENUM()
enum class EMovementDirection 
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};


UCLASS()
class SNAKEGAME_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(BlueprintReadOnly)
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClassPtr;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ADoubleFood> DblFoodClassPtr;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedUpFood> SpeedUpPtr;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASlowDown> SlowDownPtr;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AMine> MinePtr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int32 LastElement;

	float Speedcoefficient;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void AddSnakeElementsInTheEnd(int ElementNum = 1);

	void Move();

	FVector GiveMeCoordinates();

	UWorld* NWRLD = GetWorld();
	FTimerHandle TimerForMove;

	void RespawnNewFood();
	FVector GiveMeFoodCoordinates();

	void ResetTimer();
	bool CheckCoordinatesWithElements(float _x,float _y);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);


};
