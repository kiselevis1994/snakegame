// Fill out your copyright notice in the Description page of Project Settings.


#include "Mine.h"
#include "Snake.h"

// Sets default values
AMine::AMine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMine::BeginPlay()
{
	Super::BeginPlay();
	NWRLD->GetTimerManager().SetTimer(TimerToKill, this, &AMine::ToDestroy, 3.0, false);
}

// Called every frame
void AMine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AMine::Interact(AActor* interactor, bool bIsHead)
{

	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(interactor);

		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("You Got BLOWUP!!!"));
		}
	}

}

void AMine::ToDestroy()
{
	this->Destroy();
}

