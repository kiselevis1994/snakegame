// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowDown.h"
#include "Snake.h"

// Sets default values
ASlowDown::ASlowDown()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowDown::BeginPlay()
{
	Super::BeginPlay();
	NWRLD->GetTimerManager().SetTimer(TimerToKill, this, &ASlowDown::ToDestroy, 5.0, false);
}

// Called every frame
void ASlowDown::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowDown::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(interactor);

		if (IsValid(Snake))
		{
			Snake->Speedcoefficient = Snake->Speedcoefficient * 2;
			this->Destroy();
			Snake->ResetTimer();
		}
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("SlowDown X2!"));
		}
	}
}

void ASlowDown::ToDestroy()
{
	this->Destroy();
}

