// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	SetActorRotation(FRotator(-90,0,0));
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	CreateSnakeActor();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("WMapping", IE_Pressed, this, &APlayerPawnBase::WMappingCpp);
	PlayerInputComponent->BindAction("AMapping", IE_Pressed, this, &APlayerPawnBase::AMappingCpp);
	PlayerInputComponent->BindAction("SMapping", IE_Pressed, this, &APlayerPawnBase::SMappingCpp);
	PlayerInputComponent->BindAction("DMapping", IE_Pressed, this, &APlayerPawnBase::DMappingCpp);
//	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass,FTransform());
}

void APlayerPawnBase::WMappingCpp()
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			SnakeActor->Move();
			SnakeActor->ResetTimer();

			
		}
	}
	/*if (IsValid(SnakeActor)) 
	{
		if (value>0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &APlayerPawnBase::MoveSnake, 0.5, true);
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;	
	
			NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &APlayerPawnBase::MoveSnake, 0.5, true);
		}
	}
	*/
}

void APlayerPawnBase::AMappingCpp()
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->Move();
			SnakeActor->ResetTimer();

			
		}
	}
}

void APlayerPawnBase::SMappingCpp()
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->Move();
			SnakeActor->ResetTimer();	
		}
	}
}

void APlayerPawnBase::DMappingCpp()
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			SnakeActor->Move();
			SnakeActor->ResetTimer();
		}
	}
	/*if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			
			NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &APlayerPawnBase::MoveSnake, 0.5, true);
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			
			NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &APlayerPawnBase::MoveSnake, 0.5, true);
		}
	}
	*/
}

/*void APlayerPawnBase::MoveSnake()
{
	SnakeActor->Move();
}*/



