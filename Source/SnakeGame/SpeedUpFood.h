// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "SpeedUpFood.generated.h"

class ASnake;


UCLASS()
class SNAKEGAME_API ASpeedUpFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedUpFood();
	UWorld* NWRLD = GetWorld();
	FTimerHandle TimerToKill;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual	void Interact(AActor* interactor, bool bIsHead) override;

	void ToDestroy();

};
