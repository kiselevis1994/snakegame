// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "Mine.h"
#include "DoubleFood.h"
#include "SpeedUpFood.h"
#include "SlowDown.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Interactable.h"
#include "EngineGlobals.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	Speedcoefficient = 0.6;

}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
	LastMoveDirection = EMovementDirection::DOWN;
	//SetActorTickInterval(MovementSpeed);
	NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &ASnake::Move, Speedcoefficient, true, Speedcoefficient+0.1);
}

void ASnake::ResetTimer()
{
	NWRLD->GetTimerManager().SetTimer(TimerForMove, this, &ASnake::Move, Speedcoefficient, true, Speedcoefficient+0.1);
}

bool ASnake::CheckCoordinatesWithElements(float _x,float _y)
{
	for (int i =0; i < SnakeElements.Num()-1;i++)
	{
		if (_x == SnakeElements[i]->GetActorLocation().X && _y == SnakeElements[i]->GetActorLocation().Y)
		{
			return false;
			break;
		}
	}
	return true;
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//Move();
}



void ASnake::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) 
	{
		
			FVector NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

			//	NewSnakeElem->SetActorHiddenInGame(true);

			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType(); // ������ ������ ��������, ������������� ��� ������� � BP
			}
	}
}
//��������� ��������� ��� ����� ������ ������
FVector ASnake::GiveMeCoordinates()
{
	float XOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X;
	float YOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y + ElementSize;

	FVector NewElementLocation = FVector(XOfNew, YOfNew, 0);

		if (XOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X 
			&&
			YOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y + ElementSize 
			&& CheckCoordinatesWithElements(XOfNew, YOfNew) == false) // (1)
		{
			XOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X - ElementSize;
			YOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y;
		} 
		if (XOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X - ElementSize 
			&& YOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y
			&& CheckCoordinatesWithElements(XOfNew, YOfNew) == false)  // (2)
		{
			XOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X;
			YOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y - ElementSize;
		}
		if (XOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X && YOfNew == SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y - ElementSize
			&& CheckCoordinatesWithElements(XOfNew, YOfNew) == false) // (3)
		{
			XOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X + ElementSize;
			YOfNew = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y;
		}

	
	NewElementLocation = FVector(XOfNew, YOfNew,0);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, NewElementLocation.ToString());
	}

	return NewElementLocation;
}



void ASnake::RespawnNewFood()
{
	FVector NewLocation = GiveMeFoodCoordinates();
	FTransform NewTransform(NewLocation);
	AFood* NewFoodElem = GetWorld()->SpawnActor<AFood>(FoodClassPtr, NewTransform);
	//NewFoodElem->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	if ((1+rand() %5 )==2) 
	{
		NewLocation = GiveMeFoodCoordinates();
		NewTransform=FTransform(NewLocation);
		ADoubleFood* NewDblFoodElem= GetWorld()->SpawnActor<ADoubleFood>(DblFoodClassPtr, NewTransform);
		//NewDblFoodElem->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	}
	if ((1 + rand() % 7) == 3)
	{
		NewLocation = GiveMeFoodCoordinates();
		NewTransform = FTransform(NewLocation);
		ASpeedUpFood* NewSpeedUpFoodElem = GetWorld()->SpawnActor<ASpeedUpFood>(SpeedUpPtr, NewTransform);
		//NewSpeedUpFoodElem->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	}
	if ((1 + rand() % 7) == 3)
	{
		NewLocation = GiveMeFoodCoordinates();
		NewTransform = FTransform(NewLocation);
		ASlowDown* NewSlowDownElem = GetWorld()->SpawnActor<ASlowDown>(SlowDownPtr, NewTransform);
		//NewSpeedUpFoodElem->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	}
	if ((1 + rand() % 10) == 1)
	{
		NewLocation = GiveMeFoodCoordinates();
		NewTransform = FTransform(NewLocation);
		AMine* NewMineElem = GetWorld()->SpawnActor<AMine>(MinePtr, NewTransform);
		//NewSpeedUpFoodElem->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	}


}
//��������� ��������� ��� ���
FVector ASnake::GiveMeFoodCoordinates()
{
	float XOfNew = 0.0;
	float YOfNew = 0.0;
	FVector NewFoodLocation = FVector(XOfNew, YOfNew, 0);

	
	XOfNew = 57 * (-8 + rand() % 8);
	YOfNew = 57 * (-8 + rand() % 8);

	for (int i=0;i<SnakeElements.Num()-1;i++) 
	{
		if (SnakeElements[i]->GetActorLocation().X == XOfNew && SnakeElements[i]->GetActorLocation().Y == YOfNew)
		{
			XOfNew = 57 * (-8 + rand() % 8);
			YOfNew = 57 * (-8 + rand() % 8);
			i = 0;
		}
		else
		{
			NewFoodLocation = FVector(XOfNew, YOfNew, 0);
		}
	}

	return NewFoodLocation;
}



//���������� ����� ��������� � ����� ������
void ASnake::AddSnakeElementsInTheEnd(int ElementNum)
{

	for (int i = 0; i < ElementNum; i++)
	{
		FVector NewLocation = GiveMeCoordinates();
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElem);

		FString LastLocationX = FString::SanitizeFloat(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X);
		LastElement = SnakeElements.Num()-1;

	}

}


void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(-90,0,0));
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(90, 0, 0));
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0, 0, -90));
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		SnakeElements[0]->SetActorRotation(FRotator(0, 0, 90));
		break;

	}
	
	SnakeElements[0]->ToggleCollision();

	//���������� �������� ������ �� � �������
	for (int i = SnakeElements.Num() - 1; i > 0; i--) 
	{
		auto CurrentElement = SnakeElements[i]; // �������� ������� �����
		auto PrevElement = SnakeElements[i - 1]; // �������� ���������� �����
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}


void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface) 
		{
			InteractableInterface->Interact(this, bIsFirst);
		}

	}
}

