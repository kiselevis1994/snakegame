// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpFood.h"
#include "Snake.h"
#include "EngineGlobals.h"

// Sets default values
ASpeedUpFood::ASpeedUpFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedUpFood::BeginPlay()
{
	Super::BeginPlay();
	NWRLD->GetTimerManager().SetTimer(TimerToKill, this, &ASpeedUpFood::ToDestroy, 5.0, false);
}


// Called every frame
void ASpeedUpFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedUpFood::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(interactor);

		if (IsValid(Snake))
		{
			Snake->Speedcoefficient = Snake->Speedcoefficient / 2;
			this->Destroy();
			Snake->ResetTimer();
		}
		if (GEngine) 
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("SpeedUp X2!"));
		}
	}
}

void ASpeedUpFood::ToDestroy() 
{
	this->Destroy();
}

